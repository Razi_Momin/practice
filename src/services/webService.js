import axios from 'axios';
import querystring from 'querystring';
const baseApiUrl = 'http://localhost/api/';

export const webService = {
    get,
    post,
    put,
    deleteDetail,
    getHeaders
};

function get(apiEndpoint, headers) {
    return axios
        .get(baseApiUrl + apiEndpoint, { headers })
        .then((response) => {
            return response;
        })
        .catch((err) => {
            return ErrorRespond(err);
        });
}

function post(apiEndpoint, payload, headers) {
    // console.log(querystring.stringify(payload));
    // return false;
    return axios
        .post(baseApiUrl + apiEndpoint, querystring.stringify(payload), { headers })
        .then((response) => {
            return response;
        })
        .catch((err) => {
            return ErrorRespond(err);
        });
}

function put(apiEndpoint, payload, headers) {
    return axios
        .put(baseApiUrl + apiEndpoint, payload, { headers })
        .then((response) => {
            return response;
        })
        .catch((err) => {
            return ErrorRespond(err);
        });
}

function deleteDetail(apiEndpoint, headers) {
    return axios
        .delete(baseApiUrl + apiEndpoint, { headers })
        .then((response) => {
            return response;
        })
        .catch((err) => {
            return ErrorRespond(err);
        });
}

function ErrorRespond(error) {
    return { data: { type: 'ERROR', message: error, status: 'NET_ERROR' } };
}


function getHeaders(otherHeader = []) {
    const auth = JSON.parse(localStorage.getItem('auth'));
    let key = auth ? auth.key : '';
    let token = auth ? auth.token : '';
    let ppm_id = auth ? auth.ppm_id : 0;
    let options = {
        // // Accept: 'application/json',
        // Accept: 'application/json/plain,*/*',
        // "Content-Type": "application/x-www-form-urlencoded",
        'Client-Service': 'mobile-client-qc',
        'Auth-key': 'playerzpotrestapi',
        'ID': ppm_id,
        'Platform': 1,
        'Authorization': token,
        'PPM-API-KEY': key,
        'Game-Mode': 1
    };
    // console.log(options);
    options = { ...otherHeader, ...options };
    return options;
}