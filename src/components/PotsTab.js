import React, { useState } from 'react'
import CountDown from '../common/CountDown';

const PotsTab = (props) => {
    // console.log(props);
    const { startTime, team1Name, team2Name } = props.matchDetails;
    const [tabActive, setTabActive] = useState('active');
    const { tabId } = props;
    const switchTab = (id) => {
        props.changeId(id);
    }
    const posts = [
        { id: 1, name: 'all pots' },
        { id: 2, name: 'joined pots' },
        { id: 3, name: 'mysquads' }
    ];
    const content = posts.map((post) =>
        <div key={post.id} onClick={() => switchTab(post.id)} className={tabId === post.id ? `${tabActive}` : ''}>
            {post.name}
        </div>
    );
    return (
        <div className="pottabmain">
            <div className="tabtimerdiv f-12">
                <div>
                    {<CountDown time={startTime} />}
                </div>
                <div>
                    {`${team1Name} VS ${team2Name}`}
                </div>
            </div>
            <div className="bg-white d-flex justify-content-between text-uppercase potstabpparent f-12 text-center">
                {content}
            </div>
        </div>
    )
}

export default PotsTab

