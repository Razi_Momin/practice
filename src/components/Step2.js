import React, { useState, useEffect } from 'react'
import { withRouter } from "react-router-dom";
import StateList from './StateList';
import { webService } from '../services/webService';
import ReferralModal from './modals/ReferralModal';
import { Button } from 'react-bootstrap';
var md5 = require('md5');
// console.log(md5('razimomin'));
const Step2 = withRouter((props) => {
    // console.log(props.location.state)
    const [stateList, setStateList] = useState([]);
    const [stateValue, setStateValue] = useState(0);
    const [password, setPassword] = useState('');
    const [referralCode, setReferralCode] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [validReferralCode, setValidReferralCode] = useState('');
    let userId = '';
    if (props.location.state === undefined) {
        props.history.push('/step1');
        return false;
    }
    const { email, mobile_no } = props.location.state;
    useEffect(() => {
        let apiEndpoint = 'Oauth/getStates';
        let header = webService.getHeaders({})
        webService.get(apiEndpoint, header).then((response) => {
            if (response.status === 200) {
                setStateList(response.data.states);
            }
        });
    }, []);
    const Step2Submit = () => {
        if (stateValue === 0)
            alert('Please enter valid state');
        else if (password === '' || password.length < 8)
            alert('Please enter valid password');
        else
            callStep2();
    }
    const callStep2 = () => {
        // let apiEndpoint = 'Oauth/signupStepOne';
        // let header = webService.getHeaders({})
        let postArray = {
            mobile_no,
            email,
            is_email_edit: 1,
            is_mobile_change: 0,
            user_id: userId,
            state_id: stateValue,
            referral_code: referralCode,
            social_id: '',
            password: md5(password),
            encoded_password: window.btoa(password),
            login_with: 1,
            utm_source: 'main_web',
            tracking_id: 1,
            token: ''
        };
        let header = webService.getHeaders({})
        // //console.log(postArray)
        const apiEndpoint = 'Oauth/signup';
        webService.post(apiEndpoint, postArray, header).then((response) => {
            // console.log(response);
            if (response.status === 200) {
                response = response.data;
                if (response.success && response.status_code === 4) {
                    // console.log(response);
                    userId = response.user_id;
                    props.history.push({
                        pathname: '/step3',
                        state: { mobile_no, userId: userId }
                    });
                } else {
                    alert(response.message);
                }
            }
        })
    }
    return (
        <div>
            <div className="form-group">
                <label>Password</label>
                <input onChange={e => setPassword(e.target.value)} type="password" className="form-control" />
            </div>
            <div className="form-group">
                <label>State</label>
                <StateList className="form-control"
                    statelist={stateList}
                    stateValue
                    onChangeState={e => setStateValue(parseInt(e.target.value))}
                // onChangeState={stateValueChange}
                />
            </div>
            {
                validReferralCode === '' ? <div onClick={() => setModalShow(true)}
                    style={{ color: 'blue', cursor: 'pointer' }}>Have a referral code</div> : ''
            }

            <ReferralModal
                show={modalShow}
                updateReferralCode={e => setReferralCode(e.target.value)}
                referralCodeValue={referralCode}
                validreferralcode={e => setValidReferralCode(e)}
                onHide={() => setModalShow(false)}
            />
            {
                validReferralCode !== '' ? <p>Referral Code {validReferralCode} applied <span onClick={() => setModalShow(true)} style={{ color: 'red' }}>Change</span></p> : null
            }
            <div className="d-flex justify-content-between align-items-center">
                <div onClick={() => {
                    props.history.push({pathname: '/step1',state: { mobile_no: mobile_no }})
                }}>
                    Back
                </div>
                <div>
                    <Button onClick={Step2Submit}>
                        Submit
                    </Button>
                </div>
            </div>
        </div>
    )
})
export default Step2


