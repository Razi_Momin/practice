import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import { webService } from '../services/webService';

const Step3 = (props) => {
    // console.log(props);
    const [otp, setOtp] = useState('');
    const { mobile_no, userId } = props.location.state;
    const otpSubmit = (e) => {
        e.preventDefault();
        if (otp === "") {
            alert('Please enter valid otp')
        } else {
            let postArray = {
                user_id: userId,
                otp,
                type: 1,
                platform: 1,
                device_id: 1,
                social_id: '',
                version: 1,
                unique_key: 1,
                login_with: 1,
                is_guest: false,
                token: '',
                mobile_no
            };
            let header = webService.getHeaders({})
            const apiEndpoint = 'Oauth/verifyOTP';
            webService.post(apiEndpoint, postArray, header).then((response) => {
                // console.log(response);
                if (response.status === 200) {
                    response = response.data;
                    if (response.success) {
                        // console.log(response);
                        // props.history.push({
                        //     pathname: '/step3',
                        //     state: { mobile_no, userId: userId }
                        // });
                    } else {
                        alert(response.message);
                    }
                }
            })

        }
    }
    return (
        <div>
            <form onSubmit={otpSubmit}>
                <div className="form-group">
                    <label>Enter Otp {otp}</label>
                    <input onChange={e => setOtp(e.target.value)} type="text" className="form-control" placeholder="otp" />
                </div>
                <div className="form-group text-center">
                    <input type="submit" value="Confirm & get registered" className="btn btn-info" />
                </div>
            </form>
            <input type="button" value=" Already a member signIn" className="btn btn-outline-dark btn-block" />

        </div>
    )
}

export default Step3
