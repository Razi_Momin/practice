import React from 'react'

export default function StateList(props) {
    // console.log(statelist);
    const { statelist, onChangeState } = props;
    return (
        <select  onChange={onChangeState} className="form-control">
            <option key="0" value="0">Select State</option>
            {statelist.map(option => (
                <option key={option.id} value={option.id}>{option.name}</option>
            ))}
        </select>
    )
}
