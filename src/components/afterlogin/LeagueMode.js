import React, { useEffect, useState } from 'react'
import { webService } from '../../services/webService';
import { Validation } from '../../common/Validation';
import { Redirect } from 'react-router-dom';
import MatchList from './MatchList';
const LeagueMode = (props) => {
    const { _authenticate } = (Validation);
    const [isLogedIn, setIsLogedIn] = useState(_authenticate);
    const [leagueData, setLeagueData] = useState([]);
    useEffect(() => {
        if (!isLogedIn) props.history.push('/');
    }, [isLogedIn]);
    useEffect(() => {
        // console.log('useeffect calling');
        let apiEndpoint = 'ViewLeague/viewCricketLeagues';
        let header = webService.getHeaders({})
        webService.get(apiEndpoint, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                // console.log(response);
                setLeagueData(response.data);
            }
        });
    }, []);
    const logOut = () => {
        localStorage.clear();
        setIsLogedIn(false);
    }
    return (

        <MatchList leagueList={leagueData} />

        // <div>
        //     League Mode
        //     <button onClick={logOut}>Logout</button>
        // </div>
    )
}

export default LeagueMode
