import React from 'react'
import CountDown from '../../common/CountDown';
import { useHistory, Link, withRouter } from "react-router-dom";
const MatchList = (props) => {
    const history = useHistory();
    const { leagueList } = props;
    const gameMode = JSON.parse(localStorage.getItem('auth')).gameMode;
    return (
        <div style={{ marginTop: '10px' }}>
            {leagueList.map((val, key) => {
                const isSecondInnings = (parseInt(val.inning)) ? 'isSecondInnings' : '';
                return (
                    // <Link key={key} to={"/pots/" + val.series_id + "/" + val.match_id + '/' + val.master_team1 + '/' + val.master_team2} className={isSecondInnings + " matchListBox form-group"}>
                    <Link to={{
                        pathname: `/pots/${val.series_id}/${val.match_id}/${val.master_team1}/${val.master_team2}`,
                        state: {
                            team1Name: val.team1_shortname,
                            team2Name: val.team2_shortname,
                            startTime: val.match_start_datetime
                        }
                    }} className={isSecondInnings + " matchListBox form-group"}>
                        <div>{val.series_label}</div>
                        <div className="align-items-center d-flex justify-content-between text-center">
                            <div className="playerImageDiv">
                                <img src={global.config.cdnPath + "/" + global.config.gameModeArray[gameMode] + '/flags/' + val.team2_flag + ".png"} />
                            </div>
                            <div className="flex-grow-1">
                                <div>
                                    {<CountDown time={val.match_start_datetime} />}
                                </div>
                                <div className="d-flex flex-grow-1 justify-content-center">
                                    <div>{val.team1_shortname}</div>
                                    <span style={{ margin: '0 10px' }}>VS</span>
                                    <div>{val.team2_shortname}</div>
                                </div>
                            </div>
                            <div className="playerImageDiv">
                                <img src={global.config.cdnPath + "/" + global.config.gameModeArray[gameMode] + '/flags/' + val.team2_flag + ".png"} />
                            </div>
                        </div>
                    </Link>
                );
            })}
        </div>
    )
}
export default MatchList
