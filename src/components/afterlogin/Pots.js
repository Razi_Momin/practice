import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom";
import { webService } from '../../services/webService';
import PotsBox from './PotsBox';
import PotsTab from '../PotsTab';
const Pots = (props) => {
    // console.log(props);
    const [potData, setPotData] = useState([]);
    const [tabId, setTabId] = useState(1);
    const { matchid, seriesid, team1id, team2id } = props.match.params;
    const { startTime, team1Name, team2Name } = props.location.state;
    const handleId = (id) => {
        setTabId(id);
    }
    const [createSquadShow, setCreateSquadShow] = useState(true);
    useEffect(() => {
        getPotData();
        getJoinedData();
    }, [])
    const getJoinedData = () => {
        const apiEndpoint = 'Standing/matchDetails';
        let header = webService.getHeaders({})
        let postArray = { series_id: seriesid, match_id: matchid };
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    // console.log(response);
                } else {
                    alert(response.message);
                }
            }
        })
    }
    const getPotData = () => {
        const apiEndpoint = 'Pots/getAllPots';
        let header = webService.getHeaders({})
        let postArray = { series_id: seriesid, match_id: matchid };
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    // console.log(response);
                    setCreateSquadShow((response.total_squad > 11) ? false : true);
                    setPotData(response.data);
                } else {
                    alert(response.message);
                }
            }
        })
    }
    // console.log(props);
    let tabHtml;
    let tabArray = ['', <PotsDiv potData={potData} />, <JoinedPots />, <MySquads />];
    tabHtml = tabArray[tabId];
    return (
        <div>
            <PotsTab matchDetails={props.location.state} tabId={tabId} changeId={handleId} />
            {
                tabHtml
            }
            {
                // (createSquadShow) ? <button onClick={() => { props.history.push(`/createsquad/${seriesid}/${matchid}/${team1id}/${team2id}/1`) }} className="createSquadBtn">Create Squad</button> : null
                (createSquadShow) ? <button onClick={() => { props.history.push({ pathname: `/createsquad/${seriesid}/${matchid}/${team1id}/${team2id}/1`,state: { startTime: startTime } }) }} className="createSquadBtn">Create Squad</button> : null
            }
        </div>
    )
}
// function name first letter always should be in caps
const PotsDiv = ({ potData }) => {
    return (
        <PotsBox potData={potData} />
    )
}
const JoinedPots = () => {
    return (
        <div>fewfw</div>
    )
}
const MySquads = () => {
    return (
        <div>My Squads Pots</div>
    )
}
export default Pots
