import React from 'react'
import { ProgressBar, Button } from 'react-bootstrap';
import trophy from '../../assets/icons/trophy.png';
import trophyBlack from '../../assets/icons/pottrophyblack.png';
import trophyYellow from '../../assets/icons/trophyyellow.png';
const PotsBox = (props) => {
    // console.log(props);
    const { potData } = props;
    return (
        <div className="potMaincontainer">
            {potData.map((val, key) => {
                return (
                    <div key={key} className="potmain form-group f-12">
                        <div className="d-flex align-items-center potboxheading">
                            <div><img style={{ height: '20px', marginRight: '5px' }} src={trophy} /></div>
                            <div className="bold">{val.pot_heading}</div>
                        </div>
                        <div>
                            {
                                potData[key].pot_dataa.map((val, key) => {
                                    return (
                                        <div key={key} className="potmiddle">
                                            <div className="d-flex justify-content-between align-items-center m-b-10">
                                                <div>
                                                    Win Cash <span className="f-i winCashAmount bold ppm-green f-16">&#8377; <span>{val.winamount}</span></span>
                                                </div>
                                                <div>
                                                    <img style={{ height: "27px" }} src={trophyBlack} />
                                                </div>
                                            </div>
                                            <div className="d-flex justify-content-between">
                                                <div className="bold">
                                                    <span className="f-10 ppm-gray">Squads&nbsp; </span>
                                                    <span className="ppm-green">{val.no_of_winners} Winners</span>
                                                </div>
                                                <div>
                                                    <span className="bold">{val.no_of_users} /{val.no_of_joinees}</span>
                                                </div>
                                            </div>
                                            <div className="m-b-10">
                                                <div className="progressBar">
                                                    <div className="progressInner"></div>
                                                </div>
                                            </div>
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="d-flex align-items-center">
                                                    <div> <img style={{ height: '27px ' }} src={trophyYellow} /></div>
                                                    <div>
                                                        <div className="ppm-gray">
                                                            1st Prize
                                    </div>
                                                        <div>
                                                            {val.first_winner_prize}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <button className="ppm-round-btn btn-green">Fees <span>&#8377;</span> <span>{val.baseamount} /-</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                );
            })}
        </div>
    )
}

export default PotsBox
