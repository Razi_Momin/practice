import React from 'react'
import { Button, Modal } from 'react-bootstrap';
import { webService } from '../../services/webService';
export default function ReferralModal(props) {
    const { updateReferralCode, referralCodeValue, onHide, validreferralcode } = props;
    const referralCodeSubmit = () => {
        if (referralCodeValue !== '') {
            let apiEndpoint = 'Oauth/signupStepTwo';
            let header = webService.getHeaders({})
            let postArray = { referral_code: referralCodeValue };
            webService.post(apiEndpoint, postArray, header).then((response) => {
                if (response.status === 200) {
                    response = response.data;
                    if (response.success === true) {
                        validreferralcode(response.referral_code);
                        onHide();
                    } else {
                        validreferralcode('');
                        alert(response.message);
                    }

                }
            });
        } else alert('Please enter valid referral code');
        //  onHide();
    }
    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Referral Code {referralCodeValue}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-group">
                    <label>Enter referral code</label>
                    <input maxLength="10" defaultValue={referralCodeValue} onChange={updateReferralCode} type="text" className="form-control" placeholder="referral code" />
                    <br />
                    <div className="text-center">
                        <Button onClick={referralCodeSubmit}>Apply</Button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    )
}
