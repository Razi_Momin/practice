import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap';
export default function LoginInfo(props) {
  // console.log(props);
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          NUMBER VERIFICATION
          </Modal.Title>
      </Modal.Header>
      <Modal.Body>

        <p>
          The privileges to use the Paytm withdrawal feature will be available only on this number.You will not be able to modify the number again.
          </p>
      </Modal.Body>
      <Modal.Footer className="justify-content-between">
        <Button onClick={props.onHide}>Change</Button>
        <Button onClick={props.onOk}>Ok</Button>
      </Modal.Footer>
    </Modal>
  );
}