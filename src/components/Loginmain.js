import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router-dom'
import { webService } from '../services/webService';
import { Validation } from '../common/Validation';
var md5 = require('md5');
const Loginmain = (props) => {
    const { _authenticate } = (Validation);
    const mobile_no = props.location.state === undefined ? '' : props.location.state.mobile_no;
    const [mobileNo, setMobileNo] = useState(mobile_no);
    const [password, setPassword] = useState('');
    const [isLogedIn, setIsLogedIn] = useState(_authenticate);
    // if (props.location.state === undefined) return <Redirect to='/' />
    if (isLogedIn) return <Redirect to='/league' />
    const loginForm = (e) => {
        e.preventDefault();
        if (mobileNo === '' || mobileNo.length !== 10)
            return alert('Please enter mobile no');
        else if (password === "" || password.length < 8)
            return alert('Please enter valid password');
        else return callLogin()
    }
    const callLogin = () => {
        const apiEndpoint = 'Oauth/signin';
        let header = webService.getHeaders({})
        let postArray = { ppm_mobile: mobileNo, ppm_password: md5(password) };
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    let loginArray = {
                        is_logged_in: response.is_logged_in,
                        key: response.key,
                        ppm_id: response.ppm_id,
                        token: response.token,
                        gameMode: 1
                    }
                    localStorage.setItem("auth", JSON.stringify(loginArray));
                    setIsLogedIn(true);
                } else {
                    alert(response.message);
                }
            }
        })
    }
    return (
        <div>
            <form onSubmit={loginForm}>
                <div className="form-group">
                    <label>Mobile No {mobileNo}</label>
                    <input defaultValue={mobileNo} onChange={(e) => setMobileNo(e.target.value)} type="tel" className="form-control" />
                </div>
                <div className="form-group">
                    <label>password{password}</label>
                    <input onChange={e => setPassword(e.target.value)} type="password" className="form-control" />
                </div>
                <div className="text-center">
                    <input type="submit" value="submit" className="btn btn-success" />
                </div>
            </form>
        </div>
    );
}
export default Loginmain
