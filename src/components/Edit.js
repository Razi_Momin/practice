import React, { useContext, useState } from 'react'
import { LoginContext } from '../context/LoginContext';
const Edit = () => {
    const { LoginCheck } = useContext(LoginContext);
    const [newValue, setnewValue] = useState('change from edit');
    const { AddMobile } = useContext(LoginContext);
    const handleSubmit = (e) => {
        e.preventDefault();
        // console.log(title,author);
        AddMobile(newValue);
    }
    const hideDiv = () => {
        
    }
    return (
        <div>
            {/* <button onClick={handleSubmit}>Edit</button> */}
            <button onClick={hideDiv}>Hide div</button>
            {LoginCheck}
        </div>
    )
}
export default Edit
