import React, { useEffect, useState, useReducer } from 'react'
import { CreateSquadHeader } from './CreateSquadHeader';
import adult from '../../assets/icons/adult.png';
import { webService } from '../../services/webService';
import { PlayersData } from './PlayersData';
import { CreateTeamTab } from './CreateTeamTab';
export const CountContext = React.createContext();
const initialState = {
    firstCounter: 1,
    teamName: { teamName1: '', teamName2: '' },
    gemesDefault: '',
    teamCount: { team1Count: 5, team2Count: 6, totalPlayersCount: 0 }
};
const reducer = (state, action) => {
    console.log(action);
    switch (action.type) {
        case 'increment':
            return { ...state, firstCounter: action.value }
        case 'teamName':
            return { ...state, teamName: { teamName1: action.value.team1Name, teamName2: action.value.team2Name } }
        case 'playerCounter':
            return {
                ...state, teamCount: {
                    team1Count: action.value.teamPlayerCount1,
                    team2Count: action.value.teamPlayerCount2,
                    totalPlayersCount: action.value.activePlayersIdCount
                }
            }
        case 'gemesCount':
            return { ...state, gemesDefault: action.value }
        case 'reset':
            return initialState;
        default:
            return state;
    }
}
export const CreateSquad = (props) => {
    // console.log(props);
    const [playersList, setPlayersList] = useState([]);
    const [filterValue, dispatch] = useReducer(reducer, initialState);
    const { seriesid, matchid, team1id, team2id, squadId } = props.match.params;
    const { startTime } = props.location.state;
    useEffect(() => {
        getPlayersData();
    }, []);
    const getPlayersData = () => {
        const apiEndpoint = 'Squad/getMatchSquadPlayers';
        let header = webService.getHeaders({})
        let postArray = { series_id: seriesid, match_id: matchid, type: '', master_team1: team1id, master_team2: team2id };
        webService.post(apiEndpoint, postArray, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                if (response.success) setPlayersList(response.data);
                else alert(response.message);
            }
        });
    }
    return (
        <CountContext.Provider value={{ countState: filterValue, countDispatch: dispatch }}>
            <div>
                <CreateSquadHeader startTime={startTime} />
                <div className="m-t-10">
                    <CreateTeamTab />
                    <PlayersData playersList={playersList} matchDetails={props.match.params} />
                    <div className="text-uppercase d-flex save-next-sq text-center f-12">
                        <div className="col-50 bg-white">view squad</div>
                        <div className="col-50 text-white crete-nextbtn">Next</div>
                    </div>
                </div>
            </div>
        </CountContext.Provider>
    )
}



