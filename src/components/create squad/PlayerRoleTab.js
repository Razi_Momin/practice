import React, { useContext } from 'react'
import { CountContext } from '../create squad/CreateSquad'
export const PlayerRoleTab = (props) => {
    const countContext = useContext(CountContext)
    // console.log(props);
    const playerRoles = [[],
    [[], ['bat'], ['bowl'], ['AR'], ['WK']], //Cricket
    [[], ['raider'], ['allrounder'], ['defender']], //Kabaddi
    [[], ['fwd'], ['mid'], ['def'], ['gk']], //Football
    [[], ['fwd'], ['mid'], ['def'], ['gk']], //Hockey
    [[], ['point-guard'], ['shooting-guard'], ['small-guard'], ['power-forward'], ['center']] //NBA
    ];
    const minMaxArray = [
        [], [[], [3, 6], [3, 6], [1, 4], [1, 4]], //cricket
        [], //kabaddi
        [[], [1, 3], [3, 5], [3, 5], [1, 1]], //football
        [[], [1, 4], [1, 4], [1, 4], [1, 4]], //basketball
        [[], [2, 5], [2, 5], [1, 1], [1, 1]] //baseball
    ]
    const gameMode = JSON.parse(localStorage.getItem('auth')).gameMode;
    const { firstCounter } = countContext.countState;
    const [minPlayer, maxPlayer] = minMaxArray[gameMode][firstCounter];
    const playerRoleName = playerRoles[gameMode][firstCounter];
    return (
        <div style={{ marginTop: '20px' }}>
            <div className="align-items-center text-center bg-white d-flex justify-content-around playerrole-switch text-body bold text-uppercase f-12">
                {
                    playerRoles[gameMode].slice(1).map((val, key) => {
                        key = (key + 1)
                        const active = (firstCounter === key) ? 'active' : '';
                        return (
                            <div key={key} onClick={() => countContext.countDispatch({ type: 'increment', value: key })} className={`${active} cursor`}>
                                {val[0]}
                            </div>
                        )
                    })
                }
            </div>
            <div className="shadow bg-white text-body min-max-main">
                Select <span className="min-max-div">Min {minPlayer}</span> to <span className="min-max-div">Max {maxPlayer}</span> {playerRoleName}
            </div>
        </div>
    )
}
