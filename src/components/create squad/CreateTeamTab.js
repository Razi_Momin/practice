import React from 'react'
const posts = [
    { id: 1, name: 'all pots', className: 'flex-grow-1' },
    { id: 2, name: 'joined pots', className: 'cr-pg text-center' },
    { id: 3, name: 'mysquads', className: 'cr-pg text-center' }
];
const content = posts.map((post) =>
    <div key={post.id} className={`${post.className}`}>
        {post.name}
    </div>
);
export const CreateTeamTab = () => {
    return (
        <div className="d-flex text-uppercase text-white f-10 cr-pts-row">
            {content}
        </div>
    )
}
