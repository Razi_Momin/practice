import React, { useContext, useEffect, useState } from 'react'
import adult from '../../assets/icons/adult.png';
import { CountContext } from '../create squad/CreateSquad'
export const PlayersData = (props) => {
    const playerRoles = [[],
    [[], ['batsmen'], ['bowler'], ['allrounder'], ['wicketkeeper']], //Cricket
    [[], ['raider'], ['allrounder'], ['defender']], //Kabaddi
    [[], ['fwd'], ['mid'], ['def'], ['gk']], //Football
    [[], ['fwd'], ['mid'], ['def'], ['gk']], //Hockey
    [[], ['point-guard'], ['shooting-guard'], ['small-guard'], ['power-forward'], ['center']] //NBA
    ];
    const gameMode = JSON.parse(localStorage.getItem('auth')).gameMode;
    const minMaxValidation = [[], [3, 6], [3, 6], [1, 4], [1, 4]];
    const singleTeamValidation = [[], [7]];
    const maxCount = ['', 11, 9, 11, 11, 8, , , 9, 6, 7];
    const { seriesid, matchid, team1id, team2id, squadId } = props.matchDetails;
    const countContext = useContext(CountContext);
    const { playersList } = props;
    const [team1Name, team2Name] = [...new Set(playersList.map(x => x.team_shortname))];
    const { firstCounter, gemesDefault } = countContext.countState;
    const [activePlayersId, setActivePlayersId] = useState([]);
    const activePlayersIdCount = activePlayersId.length;
    const maxTeamPlayers = parseInt(maxCount[gameMode]);
    const [gamesValue, setGamesValue] = useState(0);
    const filteredItems = playersList.filter((playersList) => {
        return playersList.player_category === `${firstCounter}`
    })
    const maxGamesCount = 1000;
    const [arrayValue, setArrayValue] = useState({ ids: ['', 0, 0, 0, 0, 0, 0] });
    const ids = [...arrayValue.ids];
    const [teamPlayerCount1, setTeamPlayerCount1] = useState(0);
    const [teamPlayerCount2, setTeamPlayerCount2] = useState(0);
    const maximumValue = parseInt(singleTeamValidation[gameMode]);
    useEffect(() => {
        if (playersList.length > 0)
            countContext.countDispatch({ type: 'teamName', value: { team1Name, team2Name } })
    }, [playersList])
    useEffect(() => {
        // console.log(activePlayersIdCount);
        countContext.countDispatch({ type: 'gemesCount', value: maxGamesCount - gamesValue })
        countContext.countDispatch({ type: 'playerCounter', value: { teamPlayerCount1, teamPlayerCount2, activePlayersIdCount } })
    }, [activePlayersId]);
    const selectPlayer = (playerId) => {
        let playerIdInt = parseInt(playerId);
        const [filteredItems] = playersList.filter((playersList) => {
            return playersList.match_player_id === `${playerId}`
        })
        const { series_team_id, player_category, gems } = filteredItems;
        const playerGames = parseInt(gems);
        const [, maximumValueCategory1] = minMaxValidation[player_category];
        const playerRoleName = playerRoles[gameMode][player_category];
        const playerAvailableCheck = activePlayersId.includes(playerIdInt);
        if (!playerAvailableCheck) {// new entry
            if (teamPlayerCount1 === maximumValue || teamPlayerCount2 === maximumValue) {
                alert(`You can select maximum ${maximumValue} players from each team`);
                return false;
            }
            else if (arrayValue.ids[player_category] === maximumValueCategory1) { // maximum value validation
                alert(`max ${maximumValueCategory1 + " " + playerRoleName}  are allowed`);
                return false;
            } else if ((gamesValue + playerGames) > maxGamesCount) {
                alert(`Total gems should be bellow ${maxGamesCount}`)
                return false;
            } else if (activePlayersIdCount + 1 > maxTeamPlayers) {
                alert(`Maximum ${maxTeamPlayers} players are allowed`)
                return false;
            }
            else {
                setGamesValue(gamesValue + playerGames);
                ids[player_category] = ids[player_category] + 1;
                setArrayValue({ ids });
                // console.log(arrayValue[1]);
                if (series_team_id === `${team1id}`) {
                    setTeamPlayerCount1(teamPlayerCount1 + 1);
                } else if (series_team_id === `${team2id}`) {
                    setTeamPlayerCount2(teamPlayerCount2 + 1);
                }
            }
            // (series_team_id === `${team1id}`) ? setTeamPlayerCount1(teamPlayerCount1 + 1) : setTeamPlayerCount2(teamPlayerCount2 + 1);
            setActivePlayersId([...activePlayersId, playerIdInt]);
        }
        else { // remove entry
            const indexValue = activePlayersId.indexOf(playerIdInt);
            if (indexValue > -1) {
                setGamesValue(gamesValue - playerGames);
                ids[player_category] = ids[player_category] - 1;
                setArrayValue({ ids });
                activePlayersId.splice(indexValue, 1);
                // setActivePlayersId(activePlayersId);
                setActivePlayersId([...activePlayersId]);
                if (series_team_id === `${team1id}`) {
                    setTeamPlayerCount1(teamPlayerCount1 - 1);
                } else if (series_team_id === `${team2id}`) {
                    setTeamPlayerCount2(teamPlayerCount2 - 1);
                }
            }
        }
    }
    return (
        <>
            <div>
                {
                    filteredItems.map((val, key) => {
                        // console.log(activePlayersId);     
                        const playerAvailableCheck = activePlayersId.includes(parseInt(val.match_player_id));
                        // console.log(playerAvailableCheck);
                        const active = (playerAvailableCheck) ? 'active' : '';
                        // console.log(activePlayersId);
                        return (
                            <div onClick={() => selectPlayer(val.match_player_id, val.player_category)} key={key} className={`d-flex align-items-center bg-white cr-sq-player-row ${active}`}>
                                <div className="flex-grow-1">
                                    <div className="d-flex align-items-center">
                                        <div className="cr-ply-img">
                                            <img className="cr-sq-plimg" src={adult} />
                                        </div>
                                        <div className="flex-grow-1 f-10">
                                            <div>{val.player_shortname}</div>
                                            <div>Picked by <span>{val.selected_percentage}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="cr-pg text-center">
                                    {val.score}
                                </div>
                                <div className="cr-pg text-center">
                                    {val.gems}
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
}
