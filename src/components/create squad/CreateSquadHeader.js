import React, { useContext } from 'react'
import { DropdownButton, Dropdown } from 'react-bootstrap';
import diamond from '../../assets/icons/diamond.svg';
import { PlayerRoleTab } from './PlayerRoleTab';
import { CountContext } from '../create squad/CreateSquad'
import CountDown from '../../common/CountDown';
export const CreateSquadHeader = (props) => {
    const { startTime } = props;
    const countContext = useContext(CountContext)
    console.log(countContext);
    const { gemesDefault, teamCount } = countContext.countState;
    const { teamName1, teamName2 } = countContext.countState.teamName;
    const maxCount = ['', 11, 7, 11];
    const gameMode = JSON.parse(localStorage.getItem('auth')).gameMode;
    const maxTeamPlayers = parseInt(maxCount[gameMode]);
    return (
        <>
            <div className="create-t-header">
            </div>
            <div className="text-white">
                <div className="d-flex align-items-center p-t-10">
                    <div className="backIcon relative">
                    </div>
                    <div className="f-12">
                        <DropdownButton id="cr-dropdown" title="Dropdown button">
                            <Dropdown.Item>Action</Dropdown.Item>
                            <Dropdown.Item>Another action</Dropdown.Item>
                            <Dropdown.Item>Something else</Dropdown.Item>
                        </DropdownButton>
                    </div>
                </div>
                <div style={{ margin: '15px 0' }} className="text-center ppm-blue">
                    {<CountDown time={startTime} />}
                </div>
                <div className="d-flex justify-content-around align-items-center">
                    <div className="flex-grow-1">
                        <div className="ct-teamNameData d-flex justify-content-between">
                            <div>{teamName1}</div>
                            <div>{teamCount.team1Count}</div>
                        </div>
                    </div>
                    <div className="text-center" style={{ width: '100px' }}>
                        {teamCount.totalPlayersCount} / {maxTeamPlayers}
                    </div>
                    <div className="flex-grow-1">
                        <div className="ct-teamNameData d-flex justify-content-between">
                            <div>{teamName2}</div>
                            <div>{teamCount.team2Count}</div>
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between align-items-center m-t-10">
                    <div>
                        <div className="cr-resetbutton">Reset</div>
                    </div>
                    <div className="align-items-center d-flex gamesDiv">
                        <div className="f-12">Games left</div>
                        <div style={{ margin: '0 5px' }} className="diamond-icons">
                            <img style={{ width: '15px' }} src={diamond} />
                        </div>
                        <div style={{ color: '#00eecf' }}>{gemesDefault}</div>
                    </div>
                </div>
                <PlayerRoleTab />
            </div>
        </>
    )
}
