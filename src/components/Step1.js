import React, { useState } from 'react';
import { Validation } from '../common/Validation';
import LoginInfo from './modals/LoginInfo';
import { withRouter } from "react-router-dom";
import { webService } from '../services/webService';

const Step1 = withRouter((props) => {
    const mobile_no = props.location.state === undefined ? '' : props.location.state.mobile_no;
    const [modalShow, setModalShow] = useState(false);
    const [mobileNo, setMobileNo] = useState(mobile_no);
    const [email, setEmail] = useState('');
    const { _isValidEmailAddress } = (Validation);
    const handleSubmit = (e) => {
        e.preventDefault();
        if (mobileNo === '' || mobileNo.length !== 10) {
            alert('Please enter valid mobile no');
        } else if (email === "" || !_isValidEmailAddress(email)) {
            alert('Please enter valid email');
        } else {
            setModalShow(true)
        }
    }
    const moveStepTwo = () => {
        let apiEndpoint = 'Oauth/signupStepOne';
        let header = webService.getHeaders({})
        let postArray = { mobile_no: mobileNo, email, is_email_edit: 1 };
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                if (response.success && response.status_code === 2) {
                    props.history.push({
                        pathname: '/step2',
                        state: { mobile_no: mobileNo, email: email }
                    });
                } else {
                    alert(response.message);
                }
            }
        })
    }
    return (
        <div>
            {/* <p>{valuechack}</p> */}
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label>Mobile no</label>
                    <input maxLength="10" onChange={e => setMobileNo(e.target.value)} type="text" defaultValue={mobile_no} className="form-control" />
                </div>
                <div className="mb-3">
                    <label>Email</label>
                    <input onChange={(e) => setEmail(e.target.value)} type="email" className="form-control" />
                </div>
                <div className="text-center">
                    <button className="btn btn-info">Submit</button>
                </div>
            </form>
            {/* {
                infoModal ? <LoginInfo  data={{ handleClose }} /> : null
            } */}

            <LoginInfo show={modalShow} onOk={() => moveStepTwo()}
                onHide={() => setModalShow(false)} />
        </div>

    )
})
export default Step1
