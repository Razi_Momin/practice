import React, { useContext, useState } from 'react'
import { webService } from '../services/webService';
import { Redirect } from "react-router-dom";
import querystring from 'querystring';
import { withRouter } from "react-router-dom";
// import { LoginContext } from '../context/LoginContext';
const UserCheck = withRouter((props) => {
    // const { AddMobile } = useContext(LoginContext);
    const [mobileNo, setMobileNo] = useState('');
    const [UserExist, setUserExist] = useState('');
    const handleSubmit = (e) => {
        e.preventDefault();
        // console.log(title,author);
        // AddMobile('change from user check');
        if (mobileNo != '') {
            let apiEndpoint = 'Oauth/primaryStep';
            let header = webService.getHeaders({})
            let postArray = { mobile_no: mobileNo };
            //console.log(postArray)
            webService.post(apiEndpoint, postArray, header).then((response) => {
                if (response.status === 200) {
                    response = response.data;
                    // console.log(response);
                    if (response.success == true) {
                        if (response.status_code === 1) {
                            props.history.push({
                                pathname: '/loginmain',
                                state: { mobile_no: mobileNo }
                            });
                        } else {
                            props.history.push({
                                pathname: '/step1',
                                state: { mobile_no: mobileNo }
                            });
                        }
                    }
                } else {
                    alert('Net connection error');
                }
            })
        } else {
            alert('Please enter valid mobile no');
        }
    }
    const handleClick = () => {
        props.history.push('/loginmain');
    }
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" maxLength="10" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} placeholder="Enter mobile no"></input>
                <button>Submit</button>
            </form>
            <button onClick={handleClick}>check</button>
        </div>
    )
})
export default UserCheck