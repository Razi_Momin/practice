import React from 'react';
import UserCheck from './components/UserCheck';
// import Edit from './components/Edit';
import { BrowserRouter, Route } from 'react-router-dom';
import Step1 from './components/Step1';
import Loginmain from './components/Loginmain';
import Step2 from './components/Step2';
import Step3 from './components/Step3';
import LeagueMode from './components/afterlogin/LeagueMode';
import Pots from './components/afterlogin/Pots';
import { CreateSquad } from './components/create squad/CreateSquad';
// import Step2 from './components/Step2';

// import LoginContextProvider from './context/LoginContext';


function App(props) {

  return (
    <div className="container mainContainer">
      {/* <LoginContextProvider> */}
      {/* <UserCheck /> */}
      {/* <Edit /> */}
      {/* </LoginContextProvider> */}

      <BrowserRouter>
        <Route exact path="/" component={UserCheck} />
        <Route path="/step1" component={Step1} />
        <Route path="/step2" component={Step2} />
        <Route path="/step3" component={Step3} />
        <Route path="/league/" component={LeagueMode} />
        <Route path="/loginmain" component={Loginmain} />
        <Route path="/createsquad/:seriesid/:matchid/:team1id/:team2id/:squadId" component={CreateSquad} />
        <Route path="/pots/:seriesid/:matchid/:team1id/:team2id" component={Pots} />
      </BrowserRouter>
    </div>
  );
}

export default App;
