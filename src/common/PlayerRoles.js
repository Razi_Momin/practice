export const PlayerRoles = {
    _playerroles
};

function _authenticate(emailAddress) {
    const auth = JSON.parse(localStorage.getItem('auth'));
    return auth ? auth.is_logged_in : false;
}

